import fs from 'fs';
import {sumar, restar, multiplicar, dividir} from './calculator.js';


let resultados = [];

resultados.push("2 + 3 = " + sumar(2,3));
resultados.push("6 + 3 = " + restar(6,3));
resultados.push("6 x 3 = " + multiplicar(6,3));
resultados.push("6 x 3 = " + dividir(6,3));
resultados.push("6 x 0 = " + dividir(6,0));


resultados.forEach(function(elemento){
	console.log(elemento);
});

resultados.forEach(function(elemento){
	fs.appendFileSync("calculacion.log", elemento + "\n");
});

