const express = require('express');
const morgan = require("morgan")
const app = express();

app.use(morgan("dev")); //muestra los requests en consola

// esto es para obtener las variables por post
app.use( express.json() );       // to support JSON-encoded bodies
app.use(express.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 


// inicia servidor. El callback no es requerido
app.listen(3000, function () {
	console.log('Escuchando el puerto 3000!');
});




class Perro {
	constructor(nombre, peso, tamanio, color) {
		this.nombre = nombre;
		this.peso = peso;
		this.tamanio = tamanio;
		this.color = color;
	}
}


let listaPerros = [];
listaPerros.push(new Perro("perrito1", 50,"grande","negro"));
listaPerros.push(new Perro("perrito2", 10,"chiquito","blanco"));


let listadoCelulares = [];

class Celular{
	constructor(marca,gama,modelo,pantalla,sistOper,precio){
		this.marca  = marca;
		this.gama   = gama;
		this.modelo = modelo;
		this.pantalla = pantalla;
		this.os       = sistOper;
		this.precio   = precio;
	}

	getPrecio(){
		return this.precio;
	}
}


listadoCelulares.push(new Celular("Hyundai","baja",  "Live 2","5.7","Android 7",15000));
listadoCelulares.push(new Celular("LG",     "media", "G3","5","Android 9",8000));
listadoCelulares.push(new Celular("Samsung","alta",  "S10","7","Android 9",120000));
listadoCelulares.push(new Celular("Xiaomi", "media", "S4","5","Android 10",100000));
listadoCelulares.push(new Celular("Motorola","alta", "M50","7","Android 9",180000));




// --------------------
//      routing
app.get('/', function (req, res) {
  res.send('Hola mundo');
});

app.get('/time', function (req, res) {
	res.send(new Date());
});
 
app.get('/perros', function (req, res) {
	 res.jsonp(listaPerros); // devolver json
	 
	 /*
	 res.send(listaPerros); // texto
	 // o lista html
	 let response = "<ul>";

	 listaPerros.forEach(function(perro){
		response = response + "<li>" + perro.nombre + "</li>"
	})
	response = response + "</ul>"
	res.send(response);
	*/
	
});


app.get("/perros/:id", (req, res) => {

	if (req.params.id <= listaPerros.length -1 ) {
		res.json(listaPerros[req.params.id]);
	}
	else {
		res.json({error: "not found"});
	}

});

app.delete("/perros/:id", (req, res) => {

	if (req.params.id <= listaPerros.length -1 ) {
		listaPerros.splice(req.params.id, 1);
		res.json(listaPerros);
	}
	else {
		res.json({error: "not found"});
	}
});

app.post("/perros", (req, res) => {
	
	console.log(req.body.variables.param1);
	res.json({error:  "no implement"});
	
});


app.get("/celulares/listar", function(req, res) {
	res.json(listadoCelulares);
});



app.get('/celulares/listar-mitad', (req, res) =>{
	let cantidad = listadoCelulares.length;
	cantidad = Math.ceil(cantidad / 2);
	res.json(listadoCelulares.slice(0, cantidad));
});


app.get('/celulares/precio-bajo', (req, res) =>{
	
	let celularBajoPrecio = 999999999999999;
	let celularBajo;

	for (let i=0; i< listadoCelulares.length; i++) {
		if (listadoCelulares[i].precio < celularBajoPrecio) {
			celularBajoPrecio = listadoCelulares[i].precio;
			celularBajo = listadoCelulares[i];
		}
	}



	res.json(celularBajo);
});



app.get('/celulares/precio-alto', (req, res) =>{
	
	let celularAltoPrecio = 0;
	let celularAlto;

	for (let i=0; i< listadoCelulares.length; i++) {
		if (listadoCelulares[i].precio > celularAltoPrecio) {
			celularAltoPrecio = listadoCelulares[i].precio;
			celularAlto = listadoCelulares[i];
		}
	}



	res.json(celularAlto);
});


app.get('/celulares/listar-gamas', (req, res) =>{
	
	let listaGamaBaja  = [];
	let listaGamaMedia = [];
	let listaGamaAlta  = [];

	listadoCelulares.forEach(function(celular) {
		if (celular.gama == "baja") {
			listaGamaBaja.push(celular);
		}
	});

	listadoCelulares.forEach(function(celular) {
		if (celular.gama == "media") {
			listaGamaMedia.push(celular);
		}
	});

	listadoCelulares.forEach(function(celular) {
		if (celular.gama == "alta") {
			listaGamaAlta.push(celular);
		}
	});

	res.json({baja: listaGamaBaja, media: listaGamaMedia, alta: listaGamaAlta});
});
