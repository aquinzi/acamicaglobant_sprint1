const express = require('express');
const app = express();


app.listen(5000, () => console.log("listening on 5000"));


const esAdministrador = (req, res, next) => {
    
	const usuarioAdministrador = false;

	if(usuarioAdministrador ) {
		console.log('El usuario está correctamente logueado.');
		next();

	} else {
		res.send('No está logueado');
	}
};



//req.path para mostrar información del request

app.get('/estudiantes', esAdministrador, (req, res) => {
	res.send([
	  { id: 1, nombre: "Lucas", edad: 35 }
	])
 }); 
 
 app.post('/estudiantes', (req, res) => {
	res.status(201).send();
 });



const logger = (req, res, next) => {
	console.log(`request HTTP method: ${req.method}`);
	next();
}
app.use(logger);




// -----------------------------
//     personas array - meeting
let personasMeeting12 = [
	{id:1, nombre: "Pepe", email: "pepe@nada.com" },
	{id:2, nombre: "Hugo", email: "hugo@nada.com" },
	{id:3, nombre: "Juan", email: "juan@nada.com" },
];


function middleRuta(req, res, next) {

	console.log(req.url);
	next();
}


app.use(middleRuta); // porque se va a usar en todas las rutas


app.get("/persona/:id", (req, res) => {
	let presonaId = req.params.id;

	/*
	if (presonaId > 0 && presonaId < personasMeeting12.length ) {
		personasMeeting12.forEach(function(persona){
			if (persona.id == presonaId) {
				res.json(persona);
			}
		});
	}
	*/

	let personaEncontrada = personasMeeting12.find(persona => persona.id == presonaId);

	if (personaEncontrada) {
		res.json(personaEncontrada);
	}
	res.status(400).json({error: "no se encuentra la persona"});
});

app.get("/persona/buscar/:text", (req, res) => {
	let presonaTexto = trim(req.params.text.toLowerCase());

	personasMeeting12.forEach(function(persona){
		if (trim(persona.nombre.toLowerCase()) == presonaTexto) {
			res.json(persona);
		}
	})	;


	res.json({error: "no se encuentra la persona"});
});