const express = require('express');
const app = express();

app.use( express.json() );

app.listen(5000, () => console.log("listening on 5000"));


class Libro {
	constructor(id, titulo, descripcion, anioPublicacion) {
		this.id = id;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.anioPublicacion = anioPublicacion;
	}

	
}

class Autor {
	constructor(id, nombre, apellido, fechaDeNacimiento, libros) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaDeNacimiento = fechaDeNacimiento;
		this.libros = (libros) ? libros : [];
		
	}

	setLibros(libros) {
		this.libros = libros;
	}
}


// de prueba
let listadoAutores = [
	new Autor(1, "Jorge Luis", "Borges", "24/08/1899", [
		new Libro(1, "Ficciones", "Se trata de uno de sis más...", 1944),
		new Libro(2, "El Aleph", "Se trata de uno de sis más...", 1944),
	]),
	new Autor(2, "Julio", "Verne", "8/2/1828", [
		new Libro(1, "Veinte mil leguas de viaje submarino", "Se trata de uno de sis más...", 1869),
		new Libro(2, "La vuelta al mundo en ochenta días", "Se trata de uno de sis más...", 1872),
	]),
	new Autor(3, "Mary", "Shelley", "30/8/1797", [
		new Libro(1, "Frankenstein o el moderno Prometeo", "Se trata de uno de sis más...", 1818),
		new Libro(2, "Falkner", "Se trata de uno de sis más...", 1837),
	]),
];


function buscarIdxAutoresPorId(idBuscar) {

	for(let i = 0; i < listadoAutores.length; i++) {
		if (listadoAutores[i].id == idBuscar ) {
			return {found: true, pos: i};
		}
	}

	return {found: false, pos: -1};
}


function buscarIdxAutoresLibroPorId(idAutor, idLibro) {
	let autorPair = buscarIdxAutoresPorId(idAutor);

	for (let i = 0; i < listadoAutores[autorPair.pos].libros.length; i++ ) {
		if (listadoAutores[autorPair.pos].libros[i].id == idLibro) {
			return {found: true, pos: i};
		}
	}

	return {found: false, pos: -1};


}



// --------- middlewares ----------

function autorExistePorNombreApellido(req,res,next){ 
	let nombre = req.body.variables.nombre.toLowerCase();
	let apellido = req.body.variables.apellido.toLowerCase();

	let autorEncontrado = listadoAutores.find(persona => persona.nombre.toLowerCase() == nombre && persona.apellido.toLowerCase() == apellido);

	if (!autorEncontrado) {
		next();
	}
	else {
		res.status(402).json({message: "el autor ya se encuentra"});
	}
}


function autorExistePorId(req,res,next) {
	let id = parseInt(req.params.id);

	let autorEncontrado = listadoAutores.find(persona => persona.id == id );

	if (autorEncontrado) {
		next();
	}
	else {
		res.status(402).json({message: "el autor no se encuentra"});
	}
}

 function libroPorAutorExistePorId(req,res,next) {

	let par = buscarIdxAutoresLibroPorId(req.params.id, req.params.idLibro);

	if (par.found) {
		next();
	}
	else {
		res.status(402).json({message: "el libro no se encuentra"});
	}
 }




// --------- routing ----------
app.get("/autores", (req, res) => {
	res.json(listadoAutores);
});

app.post("/autores", autorExistePorNombreApellido, (req, res) => {
	let nombre = req.body.variables.nombre;
	let apellido = req.body.variables.apellido;
	let fechaNacimiento = req.body.variables.fechaDeNacimiento;
	let nuevoId = listadoAutores[listadoAutores.length - 1].id + 1; // asumimos que los ids son correlativos

	listadoAutores.push( new Autor( nuevoId, nombre, apellido, fechaNacimiento ) );

	res.status(200).json({message: "Autor agregado", id: nuevoId, listi: listadoAutores });
});


app.get("/autores/:id", autorExistePorId, (req, res) => {
	let autorEncontrado = listadoAutores.find(persona => persona.id ==  parseInt(req.params.id) );
	res.json(autorEncontrado);
});

app.delete("/autores/:id", autorExistePorId, (req, res) => {
	let idBuscar = parseInt(req.params.id) ;
	let idx = listadoAutores.forEach(function(autor,idxx) { 
		if (autor.id == idBuscar ) {
			return idxx;
		}
	});

	listadoAutores.splice(idx,1);
	
	res.status(200).json({message: "Autor eliminado", listi: listadoAutores });
});

// TODO: era PUT. Para sacarlo rápido lo hice con POST
app.post("/autores/:id", autorExistePorId, (req, res) => {
	let id = req.params.id;
	let nombre = req.body.variables.nombre;
	let apellido = req.body.variables.apellido;
	let fechaNacimiento = req.body.variables.fechaDeNacimiento;

	let pos = buscarIdxAutoresPorId(id);

	if (pos.found) {
		let realPos = pos.pos ;

		listadoAutores[realPos].nombre = (nombre) ? nombre : listadoAutores[realPos].nombre;
		listadoAutores[realPos].apellido = (apellido) ? apellido : listadoAutores[realPos].apellido;
		listadoAutores[realPos].fechaDeNacimiento = (fechaNacimiento) ? fechaNacimiento : listadoAutores[realPos].fechaDeNacimiento;
		
		res.status(200).json({message: "Autor modificado", listi: listadoAutores[realPos] });
	}


	res.status(402).json({message: "Autor no modificado" });
});



app.get("/autores/:id/libros", autorExistePorId, (req, res) => {
	let id = req.params.id;
	let pos = buscarIdxAutoresPorId(id);

	res.json(listadoAutores[pos.pos].libros);
});

app.post("/autores/:id/libros", autorExistePorId, (req, res) => {
	let id = req.params.id;
	let posArrayPar = buscarIdxAutoresPorId(id);

	let libroTitulo = req.body.variables.titulo;
	let libroDescripcion = req.body.variables.descripcion;
	let libroAnio = req.body.variables.anioPublicacion;
	let nuevoId = listadoAutores[posArrayPar.pos].libros[listadoAutores[posArrayPar.pos].libros.length - 1].id + 1; 


	if (posArrayPar.found) {
		listadoAutores[posArrayPar.pos].libros.push(new Libro(nuevoId, libroTitulo, libroDescripcion, libroAnio));
		res.status(200).json({message: "Libro agregado al autor modificado", listi: listadoAutores[posArrayPar.pos] });
	}

	res.status(402).json({message: "Libro no agregado" });

});


app.get("/autores/:id/libros/:idLibro", libroPorAutorExistePorId,(req, res) => {
	let idAutor = req.params.id;
	let idLibro = req.params.idLibro;

	let posArrayPar = buscarIdxAutoresPorId(idAutor);
	let posArrayParLibro = buscarIdxAutoresLibroPorId(idAutor, idLibro);

	if (posArrayParLibro.found) {
		res.status(200).json(listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos]);
	}

	res.status(402).json({message: "Libro no encontrado" });

});

//TODO: era put, se hizo con post para hacerlo rapido
app.post("/autores/:id/libros/:idLibro", libroPorAutorExistePorId, (req,res) => {
	let idAutor = req.params.id;
	let idLibro = req.params.idLibro;

	let posArrayPar = buscarIdxAutoresPorId(idAutor);
	let posArrayParLibro = buscarIdxAutoresLibroPorId(idAutor, idLibro);

	let titulo = req.body.variables.titulo;
	let descripcion = req.body.variables.descripcion;
	let anio = req.body.variables.anioPublicacion;



	if (posArrayParLibro.found) {

		listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos].titulo = (titulo) ? titulo : listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos].titulo;
		listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos].descripcion = (descripcion) ? descripcion : listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos].descripcion;
		listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos].anioPublicacion = (anio) ? anio : listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos].anioPublicacion;


		res.status(200).json(listadoAutores[posArrayPar.pos].libros[posArrayParLibro.pos]);
	}

	res.status(402).json({message: "Libro no modificado" });
});





app.delete("/autores/:id/libros/:idLibro", autorExistePorId, (req, res) => {
	let idAutor = req.params.id;
	let idLibro = req.params.idLibro;

	let posArrayPar = buscarIdxAutoresPorId(idAutor);
	let posArrayParLibro = buscarIdxAutoresLibroPorId(idAutor, idLibro);

	if (posArrayParLibro.found) {
		listadoAutores[posArrayPar.pos].libros.splice(posArrayParLibro.pos,1);

	}

	
	res.status(200).json({message: "libro eliminado", listi: listadoAutores[posArrayPar.pos] });
});